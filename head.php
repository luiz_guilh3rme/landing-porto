<!doctype html>
<html lang="pt-br">
<head>
<!--    <script src='NovoInstitucional/static_files/AppDynamics/adrum-porto.js'></script>-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Consórcio de Imóveis | Porto Seguro</title>
    <meta name="description"
          content="Com créditos de R$ 55 mil a R$ 500 mil e possibilidade de pagar em até 200 meses, você pode utilizar o crédito para adquirir um imóvel ou terreno, construir ou reformar.">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">

    <link href="common/css/ps-lib-lp-base.css" rel="stylesheet">
    <link href="common/css/ps-lib-lp-desktop-base.css" rel="stylesheet" media="all and (min-width: 768px)">

    <!--[if lte IE 9]>
    <link href="./common/css/ps-lib-lp-ie9.css" rel="stylesheet">
    <![endif]-->

    <!-- Skin: Consórcio Pesados -->

    <link rel="shortcut icon" href="https://institucional.portoseguro.com.br/visual/v.0.4/favicon/favicon.ico?v=ps-ui">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600,600i|Roboto+Slab:700"
          rel="stylesheet">
    <link href="skin/css/portoseguro-consorcio-skin.css" rel="stylesheet">


    <!--
        ### Bloco obrigatório de compatibilidade
    -->
    <!--[if lt IE 9]>
    <div class="ps-panel ps-panel-ico ps-panel-alert browserupgrade">
        <span class="ps-ico pslp-ico-alert"></span>
        <div class="ps-panel-ctt">
            Você usando um navegador <strong>desatualizado</strong>. Por favor, <a href="https://browsehappy.com/"
                                                                                   target="_blank">atualize seu
            navegador</a> para melhorar sua experiência de navegação e segurança.
        </div>
    </div>
    <![endif]-->

</head>
<body class="">
<!--
    Classes disponíveis (pode sem utilizadas juntas:
        pslp-preheader-cookie-body  -> para quando existir o cookie
        pslp-corretor-body -> para quando o topo do corretor for exibido
    -->

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TWDBBN"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TNKPKQ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->


<!--
    ### Bloco obrigatório de compatibilidade
-->
<!--[if lt IE 9]>
<div class="ps-panel ps-panel-ico ps-panel-alert browserupgrade">
    <span class="ps-ico pslp-ico-alert"></span>
    <div class="ps-panel-ctt">
        Você usando um navegador <strong>desatualizado</strong>. Por favor, <a href="https://browsehappy.com/"
                                                                               target="_blank">atualize seu
        navegador</a> para melhorar sua experiência de navegação e segurança.
    </div>
</div>
<![endif]-->

<header class="pslp-header">
    <div class="ps-container">
        <div class="ps-row">
            <div class="ps-mod8 ps-sm-mod5 ps-md-mod4 ps-sm-alignRight pslp-preheader">
                <div class="ps-row-nested pslp-preheader-ctt">
                </div>
            </div>
            <div class="ps-mod8 ps-sm-mod5 ps-md-mod4 ps-sm-alignLeft pslp-logo-area">
                <a href="/imoveis"><span class="ps-logo"></span></a>
            </div>
        </div>
    </div>
</header>
