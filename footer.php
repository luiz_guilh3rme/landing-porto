<?php require_once 'phonecta.php' ?>

<footer class="pslp-footnotes">
    <div class="ps-container">
        <div class="ps-row">
            <div class="ps-mod8 ps-sm-mod10 ps-sm-lspan1 pslp-footnotes-notes">
                <p>© <span class="ps-currentYear"></span> Porto Seguro • Todos os direitos reservados.</p>
                <p>Informações reduzidas em caráter promocional, para saber mais acesse o site <a
                        href="http://www.portoseguro.com.br/consorcio">www.portoseguro.com.br/consorcio</a>.
                    Consulte as condições gerais. Porto Seguro Administradora de Consórcios Ltda. CNPJ:
                    48.041.735/0001-90 - SAC (cancelamento e reclamações) 0800-727-2743. Atendimento para deficientes
                    auditivos 0800-727-8736 - Ouvidoria 0800-727-1184 ou (11) 3366-3184 - Central de Relacionamento
                    3366-3006 (Grande São Paulo) ou 0800-721-3006. Para saber mais sobre a Política de Privacidade da
                    Porto Seguro, <a href="http://www.portoseguro.com.br/politica-de-privacidade" target="_blank">clique
                        aqui</a>.
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    var ie9 = false;
</script>

<!--[if lte IE 9]>
<script type="text/javascript">
    ie9 = true;
</script>
<![endif]-->

<!-- LP -->
<!--<script type="text/javascript" data-main="./common/js/ps-lib-lp-boot-min" src="common/js/vendor/require.js"></script>-->
<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>

<script type="text/javascript" src="common/js/vendor/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script type="text/javascript" src="common/js/slick.js"></script>

<script src="dist/phone.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">-->
<link rel="stylesheet" href="common/css/slick.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

<!-- bulldesk -->
<script src="https://app.bulldesk.com.br/forms.js"></script>
<script>
    new BulldeskFormIntegration({
        token: 'b86544ad72b4f63f3f4b54eb257218eb',
        identifier: 'Identificador da Conversão',
        validate: true // Exibir mensagens de validação HTML5
    });
</script>


</body>
</html>
