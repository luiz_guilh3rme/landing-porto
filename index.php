<?php require_once 'head.php' ?>

<!--
    ### Módulo de "cover opening" (ou em português, bloco de abertura)
            Campos:
                - Imagens de fundo (CSS embutido): Mobile (320 x 568), Tablet vertical (768 x 1024) e Tablets horizontal e Desktops (1366 x 1024)
                - Vídeo (opcional) - pslp-video-container: Deve conter imagem da capa (poster) e o vídeo (dimensões recomendadas 640 x 360)
                - Ícone (opcional): .pslp-cover-ico - imagem SVG
                - Título: .pslp-cover-title
                - Texto (opcional): .pslp-cover-text
                - Call-to-action (opcional): .pslp-cover-cta (é necessário alterar o texto)
            -->
<style type="text/css">
    .pslp-cover-opening-model2 {
        background-image: url(instance/imovel.cover.opening.mobile.jpg)
    }

    @media all and (min-width: 768px) {
        .pslp-cover-opening-model2 {
            background-image: url(instance/imovel.cover.opening.tablet.jpg)
        }
    }

    @media all and (min-width: 990px) {
        .pslp-cover-opening-model2 {
            background-image: url(instance/imovel.cover.opening.desktop.jpg)
        }
    }

    /*# sourceMappingURL=cover.opening.model2.css.map */
</style>
<section class="pslp-cover pslp-cover-opening pslp-cover-opening-model2">
    <div class="ps-container">
        <div class="ps-row">
            <div class="ps-mod8 ps-sm-mod8 ps-sm-lspan2 ps-md-mod5 ps-md-lspan6">
                <div class="pslp-cover-card">
                    <h1 class="pslp-cover-title pslp-transition-in-item">
                        <strong>Vem fazer acontecer seu imóvel com <br class="ps-hide ps-sm-show"> o Porto Seguro <br
                                    class="ps-hide ps-sm-show">Consórcio</strong>
                    </h1>
                    <a href="javascript:;"
                       class="ps-btn ps-btn-primary pslp-cover-cta pslp-transition-in-item"
                       data-scrollto=".consorcio-simulator">Simular valor das parcelas</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pslp-features pslp-features-model1">
    <div class="ps-container">
        <div class="ps-row">
            <div class="ps-mod8 ps-sm-mod12">
                <h2 class="pslp-features-title">Por que contratar o Porto Seguro Consórcio de Imóvel?</h2>
                <p class="pslp-features-subtitle">Com créditos de R$ 55 mil a R$ 500 mil e possibilidade de pagar em até
                    200 meses, você pode utilizar o crédito <br class="ps-hide ps-md-show">para adquirir um imóvel ou
                    terreno, construir ou reformar.</p>
            </div>
        </div>
        <div class="ps-row">
            <div class="ps-mod8 ps-md-mod8 ps-md-lspan2">
                <div class="ps-row-nested pslp-features-itens"
                     data-slickconfig="{'infinite': false,'centerPadding': '2rem'}">
                    <div class="ps-sm-mod2 pslp-features-item">
                        <div class="pslp-features-card">
                            <p class="pslp-fi-ico">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80"
                                     style="enable-background:new 0 0 80 80;" xml:space="preserve">
														<use xlink:href="instance/sprite.svg#boleto"></use>
													</svg>
                            </p>
                            <h3 class="pslp-fi-title">
                                Até 200 meses <br class="ps-hide ps-md-show">para pagar
                            </h3>
                        </div>
                    </div>
                    <div class="ps-sm-mod2 pslp-features-item">
                        <div class="pslp-features-card">
                            <p class="pslp-fi-ico">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80"
                                     style="enable-background:new 0 0 80 80;" xml:space="preserve">
														<use xlink:href="instance/sprite.svg#taxas"></use>
													</svg>
                            </p>
                            <h3 class="pslp-fi-title">
                                Possibilidade de <br class="ps-hide ps-md-show">utilização do <br
                                        class="ps-hide ps-md-show">FGTS<sup>2</sup>
                            </h3>
                        </div>
                    </div>
                    <div class="ps-sm-mod2 pslp-features-item">
                        <div class="pslp-features-card">
                            <p class="pslp-fi-ico">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80"
                                     style="enable-background:new 0 0 80 80;" xml:space="preserve">
														<use xlink:href="instance/sprite.svg#contemplacao"></use>
													</svg>
                            </p>
                            <h3 class="pslp-fi-title">
                                Alto índice de <br class="ps-hide ps-md-show">contemplação
                            </h3>
                        </div>
                    </div>
                    <div class="ps-sm-mod2 pslp-features-item">
                        <div class="pslp-features-card">
                            <p class="pslp-fi-ico">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80"
                                     style="enable-background:new 0 0 80 80;" xml:space="preserve">
														<use xlink:href="instance/sprite.svg#smartphone"></use>
													</svg>
                            </p>
                            <h3 class="pslp-fi-title">
                                Lance online <br class="ps-hide ps-md-show">pelo aplicativo
                            </h3>
                        </div>
                    </div>
                    <div class="ps-sm-mod2 pslp-features-item">
                        <div class="pslp-features-card">
                            <p class="pslp-fi-ico">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80"
                                     style="enable-background:new 0 0 80 80;" xml:space="preserve">
														<use xlink:href="instance/sprite.svg#qualidade"></use>
													</svg>
                            </p>
                            <h3 class="pslp-fi-title">
                                Qualidade de <br class="ps-hide ps-md-show">atendimento <br class="ps-hide ps-md-show">Porto
                                Seguro
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="pslp-features pslp-features-model3">
    <div class="ps-container">
        <div class="ps-row">
            <div class="ps-mod8 ps-sm-mod12">
                <h2 class="pslp-features-title">E você pode usar o crédito em diversos tipos de imóvel:</h2>
            </div>
        </div>
        <div class="ps-row">
            <div class="ps-mod8 ps-md-mod10 ps-md-lspan1">
                <div class="ps-row-nested pslp-features-itens"
                     data-slickconfig="{'infinite': false,'centerPadding': '2rem'}">
                    <div class="ps-sm-mod3 pslp-features-item">
                        <div class="pslp-features-card">
												<span class="pslp-fi-img">
													<img src="instance/imovel.cover.features.model3.item1.jpg" alt=""/>
												</span>
                            <h3 class="pslp-fi-title">
                                Casa ou apartamento
                            </h3>
                        </div>
                    </div>

                    <div class="ps-sm-mod3 pslp-features-item">
                        <div class="pslp-features-card">
												<span class="pslp-fi-img">
													<img src="instance/imovel.cover.features.model3.item2.jpg" alt=""/>
												</span>
                            <h3 class="pslp-fi-title">
                                Construção ou reforma
                            </h3>
                        </div>
                    </div>
                    <div class="ps-sm-mod3 pslp-features-item">
                        <div class="pslp-features-card">
												<span class="pslp-fi-img">
													<img src="instance/imovel.cover.features.model3.item3.jpg" alt=""/>
												</span>
                            <h3 class="pslp-fi-title">
                                Imóvel de uso comercial
                            </h3>
                        </div>
                    </div>
                    <div class="ps-sm-mod3 pslp-features-item">
                        <div class="pslp-features-card">
												<span class="pslp-fi-img">
													<img src="instance/imovel.cover.features.model3.item4.jpg" alt=""/>
												</span>
                            <h3 class="pslp-fi-title">
                                Compra de terreno
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pslp-features consorcio-simulator">
    <div class="ps-container consorcio-simulator-mode">
        <div class="ps-row">
            <div class="ps-mod8 ps-sm-mod12 ps-noGutter">
                <h2 class="pslp-features-title">Simule abaixo o valor de crédito<br> que você deseja.</h2>
                <ul class="ps-row ps-tabs">
                    <li>
                        <a href="#aba-credito" class="ps-tab ps-tab-selected"><span>Escolha o valor do</span>crédito</a>
                    </li>
                    <li>
                        <a href="#aba-parcela" class="ps-tab"><span>Escolha o valor da</span> parcela</a>
                    </li>
                </ul>
                <div class="ps-tab-content">
                    <div class="ps-tab-content-item" id="aba-credito">
                        <div class="ps-row">
                            <div class="ps-mod8 ps-sm-mod6 ps-sm-lspan3 ps-md-mod4 ps-md-lspan4">
                                <h3>Selecione o valor do crédito <br class="ps-hide ps-sm-show">e saiba o valor das
                                    parcelas</h3>
                                <div class="ps-frm-select">
                                    <select class="consorcio-credito-change" data-gtm-type="click"></select>
                                </div>
                            </div>
                        </div>
                        <div class="ps-row">
                            <div class="ps-mod8 ps-sm-mod10 ps-sm-lspan1 ps-lg-mod8 ps-lg-lspan2 ps-noGutter">
                                <span class="pslp-ico-go"></span>
                                <div class="slick-1 slick-wrapper">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ps-tab-content-item" id="aba-parcela" style="display: none;">
                        <div class="ps-row">
                            <div class="ps-mod8 ps-sm-mod4 ps-sm-lspan4">
                                <h3>Valor da parcela</h3>
                                <div class="ps-frm-select">
                                    <select class="consorcio-parcela-change"></select>
                                </div>
                            </div>
                        </div>
                        <div class="ps-row">
                            <div class="ps-mod8 ps-sm-mod10 ps-sm-lspan1 ps-lg-mod8 ps-lg-lspan2 ps-noGutter">
                                <span class="pslp-ico-go"></span>
                                <div class="slick-2 slick-wrapper" style="width: 100%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pslp-cover pslp-cover-movie pslp-cover-movie-model1">
    <div class="ps-container">
        <div class="ps-row">
            <div class="ps-mod8 ps-sm-mod6 ps-sm-lspan3 pslp-csm-ctt">
                <h2 class="pslp-csm-title">
                    Veja como funciona o Consórcio
                </h2>
            </div>
            <div class="ps-mod8 ps-sm-mod8 ps-sm-lspan2 pslp-csm-frame">
                <iframe src="https://www.youtube.com/embed/4TNPi3q_tPs?rel=0&amp;showinfo=0" frameborder="0"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="pslp-faq pslp-faq-model1">
    <div class="ps-container">
        <div class="ps-row">
            <div class="ps-mod8 ps-sm-mod10 ps-sm-lspan1">
                <h2 class="pslp-faq-title">
                    Tire suas dúvidas sobre o Consórcio Imóvel
                </h2>
                <ul>
                    <li class="pslp-faq-item-opened">
                        <a href="javascript:;" class="pslp-faq-question" data-gtm-type="click"
                           data-gtm-clicktype="accordion" data-gtm-name="Como funciona o consórcio de imóveis?">Como
                            funciona o consórcio de imóveis?</a>
                        <div class="pslp-faq-content">
                            <p>Consórcio é a reunião de um grupo de pessoas que tem um objetivo em comum: comprar,
                                construir ou reformar um imóvel com parcelas acessíveis e taxas mais atrativas. O grupo
                                é administrado pela Porto Seguro Administradora de Consórcios Ltda. Cada integrante do
                                grupo se compromete a pagar, por um determinado período, uma parcela mensal que inclui o
                                valor para a aquisição do imóvel e a taxa de administração do grupo de consórcio. Ao ser
                                contemplada, a pessoa recebe uma carta de crédito para poder comprar, construir ou
                                reformar o imóvel de seu interesse. A contemplação é feita por meio de sorteios ou
                                lances que são realizados, periodicamente, nas assembleias. O sistema de consórcio é
                                regularizado e fiscalizado pelo Banco Central do Brasil (BACEN). </p>
                        </div>
                    </li>
                    <li class="pslp-faq-item-opened">
                        <a href="javascript:;" class="pslp-faq-question" data-gtm-type="click"
                           data-gtm-clicktype="accordion" data-gtm-name="Como ofertar um lance?">Como ofertar um
                            lance?</a>
                        <div class="pslp-faq-content">
                            <p>
                                O lance pode ser ofertado pelo seu aplicatitvo Porto Seguro Consórcio ou das seguintes
                                formas:&nbsp;<br>
                                - Área do Cliente, por meio do site.<br>
                                - Pessoalmente, no dia da assembleia, das 8h15 às 10h.<br>
                                - Disque Cotas (atendimento telefônico), até 30 minutos antes do inicio da assembleia,
                                pelos telefones 11 3366 3006, para Grande São Paulo, e 0800 721 3006, para demais
                                regiões.
                            </p>
                        </div>
                    </li>
                    <li class="pslp-faq-item-opened">
                        <a href="javascript:;" class="pslp-faq-question" data-gtm-type="click"
                           data-gtm-clicktype="accordion" data-gtm-name="Como posso usar meu crédito?">Como posso usar
                            meu crédito?</a>
                        <div class="pslp-faq-content">Para consórcio de imóvel, o crédito pode
                            ser utilizada para imóveis residenciais ou comerciais, novos ou usados, além de terrenos e,
                            ainda, para construções ou reformas.
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->

<div class="modal" style="display: none" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center">
                <h3>
                    Solicitar sua proposta agora
                </h3>
                <form id="formSend" class="formSend" action="obrigado-pelo-contato" method="post">
                    <div class="form-group">
                        <input type="text" name="name" class="ps-frm-entry ps-frm-valid ps-with-lightBg" placeholder="Nome" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="ps-frm-entry ps-frm-valid ps-with-lightBg" placeholder="E-mail" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="whatsapp" class="ps-frm-entry ps-frm-valid ps-with-lightBg" placeholder="Whatsapp" min="14" required>
                    </div>
                    <div>
                        <input type="hidden" name="value-credit" class="value-credit">
                    </div>
                    <div class="form-group" style="margin-top: 30px">
                        Valor do crédito: <strong style="font-size: 1.4rem"><span class="val-credit"></span></strong>
                    </div>
                    <button type="submit" class="ps-btn ps-btn-primary pslp-fm-btn pslp-form-init">Quero receber agora</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php' ?>
