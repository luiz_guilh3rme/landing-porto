<a class="whatsapp-wrapper" href="https://wa.me/15551234567" target="_blank">
    <div><i class="fa fa-whatsapp" aria-hidden="true"></i></div>
</a>

<div class="call-cta-wrapper">
    <div class="cta-tooltip">
        <p class="tooltip-text">
            Gostaria de receber uma ligação?
        </p>
        <button class="confirm">SIM</button>
        <button class="close-cta" aria-label="Fechar CTA de whatsapp">
            &times;
        </button>
    </div>
    <button class="phone-icon">
        <i class="fa fa-phone"></i>
    </button>
</div>

<div class="cta-overlay">
    <button class="close-modal close-cta-forms" aria-label="Fechar Modal">
        &times;
    </button>
    <div class="form-wrapper-all">
        <div class="form-picker">
            <button class="form-pickers" data-instance="00">
                <i class="fa fa-phone"></i>
                ME LIGUE AGORA
            </button>
            <button class="form-pickers" data-instance="01">
                <i class="fa fa-clock-o alt"></i>
                ME LIGUE DEPOIS
            </button>
            <button class="form-pickers active" data-instance="02">
                <i class="fa fa-comments alt"></i>
                DEIXE UMA MENSAGEM
            </button>
        </div>
        <div class="instance" data-instance="00">
            <div class="container">
                <div class="leave-message">
                    <legend class="leave-title">
                    <span class="variant">
                        NÓS TE LIGAMOS!
                    </span>
                        Informe seu telefone que entraremos em
                        contato o mais rápido possível.
                    </legend>
                    <div class="fields cleared">
                        <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
                    </div>
                    <form class="formSend" action="obrigado-pelo-contato" method="post">
                        <input type="hidden" name="form_type_message" value="callme_now">
                        <div class="form-group">
                            <input type="text" name="name" class="ps-frm-entry ps-frm-valid ps-with-lightBg"
                                   placeholder="Nome" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="ps-frm-entry ps-frm-valid ps-with-lightBg"
                                   placeholder="Número" required>
                        </div>
                        <button type="submit" class="ps-btn ps-btn-primary pslp-fm-btn pslp-form-init">
                            Quero receber agora
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="instance" data-instance="01">
            <div class="leave-message schedule-time">
                <legend class="leave-title">
                    Gostaria de agendar e receber uma
                    chamada em outro horário?
                </legend>
                <div class="fields cleared">
                    <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
                </div>
                <div class="container">
                    <form class="formSend" action="obrigado-pelo-contato" method="post">
                        <input type="hidden" name="form_type_message" value="callme_date">
                        <div class="form-group">
                            <input type="text" name="name" class="ps-frm-entry ps-frm-valid ps-with-lightBg"
                                   placeholder="Nome" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="ps-frm-entry ps-frm-valid ps-with-lightBg"
                                   placeholder="Número" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="datetime" class="ps-frm-entry ps-frm-valid ps-with-lightBg"
                                   placeholder="Horário" required>
                        </div>
                        <button type="submit" class="ps-btn ps-btn-primary pslp-fm-btn pslp-form-init">
                            Quero receber agora
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="instance active" data-instance="02">
            <div class="leave-message">
                <legend class="leave-title">
                    Deixe sua mensagem! Entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="fields cleared">
                    <p class="callers">Você já é a <span class="number">3</span> pessoa a deixar uma mensagem.</p>
                </div>
                <div class="container">
                    <form class="formSend" action="obrigado-pelo-contato" method="post">
                        <input type="hidden" name="form_type_message" value="message">
                        <div class="form-group">
                            <input type="text" name="name" class="ps-frm-entry ps-frm-valid ps-with-lightBg"
                                   placeholder="Nome" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="ps-frm-entry ps-frm-valid ps-with-lightBg"
                                   placeholder="Número" required>
                        </div>
                        <div class="form-group">
                            <textarea type="text" name="message" class="ps-frm-entry ps-frm-valid ps-with-lightBg"
                                      placeholder="Digite sua mensagem" required></textarea>
                        </div>
                        <button type="submit" class="ps-btn ps-btn-primary pslp-fm-btn pslp-form-init">
                            Quero receber agora
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="dist/main.css">
