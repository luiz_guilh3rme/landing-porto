window["adrum-start-time"] = new Date().getTime();
var AdrumPortoCustom = {
    appKey: {
        hml: "EUM-AAB-AWU",
        prd: "EUM-AAB-AWV"
    },
    beaconUrl: {
        hml: {
            httpContext: "http://li3450.portoseguro.brasil:7001",
            httpsContext: "https://li3450.portoseguro.brasil:7002"
        },
        prd: {
            httpContext: "http://eum.portoseguro.com.br",
            httpsContext: "https://eum.portoseguro.com.br"
        }
    },
    getStaticFilesBasePath: function() {
        return "NovoInstitucional/static_files/AppDynamics"
    },
    getAdrumFileBasePath: function() {
        return this.getStaticFilesBasePath() + "/adrum.js"
    },
    isProdEnv: function() {
        return window.location.href.indexOf(".portoseguro.com.br") > -1
    },
    getAppKeyEnv: function() {
        return this.isProdEnv() ? this.appKey.prd : this.appKey.hml
    },
    getBeaconUrlEnv: function(a) {
        if (this.isProdEnv()) {
            return a ? this.beaconUrl.prd.httpsContext : this.beaconUrl.prd.httpContext
        } else {
            return a ? this.beaconUrl.hml.httpsContext : this.beaconUrl.hml.httpContext
        }
    },
    setCookie: function(b, f, c) {
        var e = new Date();
        e.setTime(e.getTime() + (c * 24 * 60 * 60 * 1000));
        var a = "expires=" + e.toUTCString();
        document.cookie = b + "=" + f + ";" + a + ";path=/"
    },
    getCookie: function(d) {
        var b = d + "=";
        var f = decodeURIComponent(document.cookie);
        var a = f.split(";");
        for (var e = 0; e < a.length; e++) {
            var g = a[e];
            while (g.charAt(0) == " ") {
                g = g.substring(1)
            }
            if (g.indexOf(b) == 0) {
                return g.substring(b.length, g.length)
            }
        }
        return ""
    },
    parseJwt: function(c) {
        var b = c.split(".")[1];
        var a = b.replace("-", "+").replace("_", "/");
        return JSON.parse(window.atob(a))
    }
};
(function(a) {
    a.appKey = AdrumPortoCustom.getAppKeyEnv();
    a.beaconUrlHttp = AdrumPortoCustom.getBeaconUrlEnv(false);
    a.beaconUrlHttps = AdrumPortoCustom.getBeaconUrlEnv(true);
    a.adrumExtUrlHttp = AdrumPortoCustom.getStaticFilesBasePath();
    a.adrumExtUrlHttps = AdrumPortoCustom.getStaticFilesBasePath();
    a.xd = {
        enable: true
    }
})(window["adrum-config"] || (window["adrum-config"] = {}));
document.write(unescape("%3Cscript") + ' src="' + AdrumPortoCustom.getAdrumFileBasePath() + '"' + unescape("%3E%3C/script%3E"));