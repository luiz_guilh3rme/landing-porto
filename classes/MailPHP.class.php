<?php

/*
 * Developer: Pedro Lima
 * Email: pedrolion1@hotmail.com
 * Website www.3xceler.com.br / plima.me
 * Date 03/07/2019
 * */

class MailXcelerIMAP
{
    private $inputsFields = [
        ['name'                     => 'Nome'],
        ['email'                    => 'E-mail'], // not change
        ['phone'                    => 'Telefone'],
        ['whatsapp'                 => 'Whatsapp'],
        ['message'                  => 'Mensagem'],
        ['productType'              => 'Tipo de Produto'],
        ['productModel'             => 'Modelo'],
        ['datetime'                 => 'Data e Horário'],
        ['form_type_message'        => 'TIpo de formulário'],
        ['value-credit'             => 'Valor do crédito']
    ];

    private $subject;
    public $guestMail;
    public $redirectTo;

    /*
        Email configs
    */
    private $headers;
    private $message;
    private $body;

    /*
     * Caixas que irão receber o e-mail, buildado por $this->makeSendTo()
     * */
    private $mailTo;

    /*
        Servers variables
    */
    private $inputs;
    private $servers;
    private $request;
    private $serverSchema;
    private $domain;

    public function __construct(
        $client_key,
        $client_domain,
        $client_receive,
        $subject,
        $guestMail,
        $send = true,
        $redirectTo = null,
        $debug = false
    )
    {
        $this->subject = $subject;
        $this->debug = $debug;
        $this->guestMail = $guestMail;
        $this->setRedirectURL($redirectTo);

        $this->setPageKeyApi($client_key);
        $this->setClientDomain('@' . $client_domain);
        $this->setClientReceiveMails($client_receive);

        $this->captureRequest();

        if (array_key_exists('redirectTo', $this->inputs)) $this->redirectTo = $this->inputs['redirectTo'];
        if (array_key_exists('email', $this->inputs)) $this->guestMail = $this->inputs['email'];

        if ($send) $this->send();
    }

    public function setPageKeyApi($key): void
    {
        $this->page_key_api = $key;
    }

    public function setClientReceiveMails(array $mails): void
    {
        $this->mails = $mails;
    }

    public function setClientDomain($domain): void
    {
        $this->domain = $domain;
    }

    public function setCustomFieldsEmail(array $fields): void
    {
        $this->inputsFields = $fields;
    }

    public function setRedirectURL($url): void
    {
        $this->redirectTo = $url;
    }

    public function getClientReceiveMails()
    {
        return $this->mails;
    }

    public function getClientDomain(): string
    {
        return $this->domain;
    }

    public function send()
    {
        $this->sendApp3xceler();

        $this->makeMail();

        if ($this->debug) {
            var_dump([$this->mailTo, $this->subject, $this->message, $this->headers]);
            die;
        }

        mail($this->mailTo, $this->subject, $this->message, $this->headers);

        if ($this->redirectTo) {
            $locationTo = "Location: " . $this->serverSchema . '://' . $_SERVER['HTTP_HOST'] . $this->redirectTo;
            $locationTo = str_replace('www.', '', $locationTo);
            header($locationTo);
        }
    }

    public function makeMail()
    {
        $this->makeHeaders();
        $this->makeMessage();
    }

    public function makeSendTo()
    {
        foreach ($this->mails as $key => $mail)
            // Se for a última iteração
            if (!next($this->mails))
                $this->mailTo .= "{$mail}{$this->domain}";
            else
                $this->mailTo .= "{$mail}{$this->domain},";

        return $this->mailTo;
    }

    public function makeHeaders()
    {
        $this->headers = "From: " . $this->guestMail . "\r\n";
        $this->headers .= "Reply-To: " . $this->makeSendTo() . "\r\n";
        // $this->headers .= "CC: susan@example.com\r\n";
        $this->headers .= "MIME-Version: 1.0\r\n";
        $this->headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        return $this->headers;
    }

    public function makeMessage()
    {
        $this->message = '<html lang="pt-br"><body>';
        $this->message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
        $this->message .= $this->makeBody();
        $this->message .= "</table>";
        $this->message .= "</body></html>";
    }

    public function makeBody()
    {
        foreach ($this->inputsFields as $field)
            if (array_key_exists(key($field), $this->inputs))
                $this->body .= "<tr><td><strong> " . array_values($field)[0] . "</strong> </td><td>" . strip_tags($this->inputs[key($field)]) . "</td></tr>";

        return $this->body;
    }

    public function captureRequest()
    {
        $this->inputs = $_POST;
        $this->servers = $_SERVER;
        $this->request = $_REQUEST;

        if (
            array_key_exists('SERVER_PORT', $_REQUEST)
            &&
            $_SERVER['SERVER_PORT'] == 443
        )
            $this->serverSchema = 'https';
        else
            $this->serverSchema = 'http';
    }

    public function sendApp3xceler(): void
    {
        try {

            $data['name']               = $this->inputs['nome'] ?? '';
            $data['phone']              = $this->inputs['whatsapp'] ?? '';
            $data['whatsapp']           = $this->inputs['whatsapp'] ?? '';
            $data['message']            = $this->inputs['message'] ?? '';
            $data['subject']            = 'Formulário de Contato - ConsorcioPortoOnline';
            $data['api_private_key']    = $this->page_key_api;

            $url = "dashboard.3xceler.com.br/api/lead";

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_exec($ch);
            curl_close($ch);

        } catch (\Exception $e) {}
    }
}
