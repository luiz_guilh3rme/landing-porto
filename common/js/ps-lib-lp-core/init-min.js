 // @codekit-append "./ps-lib-core-includes.js";
 // @codekit-append "../vendor/svg4everybody.js";

var psLP = { 
	CTA: {
		"FloatingCTA": {},
		"FloatingCTA_start": 0
	},
	Init: function() {
		if($(".pslp-cover-opening:eq(0)").length > 0)
			psLP.CTA.FloatingCTA_start = ($(".pslp-cover-opening:eq(0) .pslp-cover-cta").offset().top + $(".pslp-cover-opening:eq(0) .pslp-cover-cta").outerHeight());

		psLP.CTA.FloatingCTA = $(".pslp-floating-cta");

		psLP.CoverOpening();
		psLP.FeatureSlide();
		psLP.CoverSlide();
		psLP.VideoBackground();
		psLP.FaqInitBinding();
		psLP.FormInitBinding();

		svg4everybody();

		$("*[data-scrollto]").on("click.pslp",function() {
			var t = $(this),
				d = t.data("scrollto");

			d = $(d);
			if(d.length > 0) {
				psLib.ScrollTo(d.offset().top,250);
			}
		});

		var year = $(".ps-currentYear");
		if(year.length) {
			var d = new Date();
			year.html(d.getFullYear());
		}

		//psLib.ModalShowHide("#pslp-form-modal",false,false,"psLP.FormBoot(modal,'Step1')","psLP.FormStop()");
	},
	CoverOpening: function() {
		$(".typeit").each(function() {
			var t = $(this),
				phrases = t.data("typeit");
			if(typeof phrases == "undefined") return;

			phrases = phrases.replace(/'/g,"\"");
			phrases = JSON.parse(phrases);

			var instance = new TypeIt(this,{
				strings: phrases,
				speed: 80,
				breakLines: false,
				autoStart: true,
				nextStringDelay: 2000,
				deleteDelay: 2500,
				startDelay: 300,
				loop: true
			});
		});
	},
	FeatureSlide: function() {
		var defaultCfg = {
				slidesToScroll: 1,
				dots: true,
				centerMode: true,
				arrows: false,
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 767,
						settings: "unslick"
					}
				]
			};

		psLP.SlickSlides(".pslp-features-itens",defaultCfg);
	},
	CoverSlide: function() {
		var defaultCfg = {
				slidesToScroll: 1,
				dots: true,
				centerMode: false,
				arrows: true,
				mobileFirst: true,
				infinite: true,
				responsive: [
					{
						breakpoint: 767,
						settings: {
							fade: true,

						}
					}
				]
			};

		psLP.SlickSlides(".pslp-cover-slide",defaultCfg);
	},
	SlickSlides: function(container,cfg) {
		$(container).each(function() {
			var t = $(this),
				setCfg = typeof t.data("slickconfig") !== "undefined" ? t.data("slickconfig") : null,
				defaultCfg = cfg;

			if(t.children("div").length < 2) return;

			if(setCfg != null) {
				setCfg = setCfg.replace(/'/g,"\"");
				setCfg = JSON.parse(setCfg);
				$.extend(defaultCfg,setCfg);
			}

			t.slick(defaultCfg);
			setTimeout(function() {
				psLib.Init(t);
			},100);
		});
	},
	ScrollingBindings: function() {
		if(typeof window.ie9 !== "undefined") {
			psLP.CTA.FloatingCTA = $(".pslp-floating-cta");
		}
		if(psLP.CTA.FloatingCTA.length == 0) return;

		var window_height = $(this).height(),
			window_top_position = $(this).scrollTop(),
			window_bottom_position = (window_top_position + window_height);

		if(window_top_position > psLP.CTA.FloatingCTA_start) {
			if(!window.ie9)
				psLP.CTA.FloatingCTA.addClass("pslp-floating-cta-on");
			else
				$(".pslp-floating-cta").addClass("pslp-floating-cta-on");
		} else {
			if(!window.ie9)
				psLP.CTA.FloatingCTA.removeClass("pslp-floating-cta-on");
			else
				$(".pslp-floating-cta").removeClass("pslp-floating-cta-on");
		}
	},
	VideoBackground: function() {
		if(psLib.IsMobile) return;

		$(".pslp-video-bg").each(function() {
			var t = $(this),
				src = t.data("source");

			if(typeof src == "undefined" || src == "") return;

			t.append('<source src="'+src+'" type="video/mp4">');
		});
	},
	FaqInitBinding: function() {
		$(".pslp-faq-question").on("click.pslp",function(e) {
			e.preventDefault();
			var li = $(this).parent(),
				ctt = $(this).next();
			
			$(this).parent().toggleClass("pslp-faq-item-opened");

			if(ctt.is(":visible"))
				ctt.slideUp("fast");
			else
				ctt.slideDown("fast");
		});
	},
	FormInitBinding: function(container) {
		if(typeof container == "undefined") container = "body";

		$(".pslp-form-init",container).on("click.pslp",function(e) {
			e.preventDefault();

			var form = "Step1";
			if($(this).is(".pslp-form-cookie-init"))
				form = "CookiePrompt";

			psLib.ModalShowHide("#pslp-form-modal",false,false,"psLP.FormBoot(modal,'"+form+"')","psLP.FormStop()"); 
		});

		$(".pslp-form-prototol-init",container).on("click.pslp",function(e) {
			e.preventDefault();

			psLib.ModalShowHide("#pslp-form-modal",false,true,"psLP.FormBoot(modal,'ProtocolForm')","psLP.FormStop()");
		});

		$(".pslp-form-cookie-form",container).on("click.pslp",function(e) {
			e.preventDefault();

			psLib.ModalShowHide("#pslp-form-modal",false,false,"psLP.FormBoot(modal,'CookieForm')","psLP.FormStop()");
		});
	},
	FormBoot: function(modal,startAt) {
		var started = typeof modal.data("formboot") == "undefined" ? false : modal.data("formboot");

		$("body").addClass("pslp-form-start");
		if(psLib.IsMobile)
			$(window).scrollTop(0);

		requirejs(["pslibForm"],function() {
			if(started) {
				var startObj = eval("psLP_form.Steps."+startAt);

				psLP_form.Modal.find(".pslp-fm-step").hide();
				psLP_form.StepEnter(startObj,null);
				return;
			}

			var psForm = new window.psForm($("#ps-frm-lead"));
			modal.data("formboot",true);
			psLP_form.Init(modal,startAt);
		});
	},
	FormStop: function() {
		$("body").removeClass("pslp-form-start");
	},
	ConsorcioSimulation: function(months,data) {
		if(typeof months == "object") months = null;
		var formatCurrency = function(str) {
			if(typeof str == "string") str = str.replace(/,/g,"");
			var n = str, 
				c = 2, 
				d = ",", 
				t = ".",
				m = "",
				ii = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
				j = (j = ii.length) > 3 ? j % 3 : 0;

			return m + (j ? ii.substr(0, j) + t : "") + ii.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - ii).toFixed(c).slice(2) : "");
		};
		var selectCredit = $(".consorcio-credito-change"),
			selectFee = $(".consorcio-parcela-change"),
			creditList = $(".pslp-fm-consorcio-credito-cotas"),
			feeList = $(".pslp-fm-consorcio-parcela-cotas"),
			selectCredit_ctt = "",
			selectFee_ctt = "",
			creditList_ctt = "",
			feeList_ctt = "",
			dataCredit = data.concat();

		data = data.sort(function(a,b) {
			return (a.fee - b.fee)
		});

		dataCredit = dataCredit.sort(function(a,b) {
			return (a.credit - b.credit)
		});


		for(var i=0,l=data.length;i<l;i++) {
			var productFee = data[i],
				productCredit = dataCredit[i],
				localMonths = months == null ? productCredit.deadline : productFee.deadline;
			
			selectCredit_ctt += '<option value="'+productCredit.credit+'" data-switch-text="R$ '+formatCurrency(productCredit.credit)+'">R$ '+formatCurrency(productCredit.credit)+'</option>';

			selectFee_ctt += '<option value="'+productFee.fee+'" data-switch-text="R$ '+formatCurrency(productFee.feePJ)+'">R$ '+formatCurrency(productFee.fee)+'</option>';

			creditList_ctt += '\
				<div>\
					<div class="pslp-fm-consorcio-cota">\
						'+(months == null ? '\
						<div class="pslp-fm-main-pf">' : '')+'\
							<div class="pslp-fm-main-highlight">\
								<label>Valor da parcela</label>\
								<strong>R$ '+formatCurrency(productCredit.fee)+'</strong>\
							</div>\
							'+(months == null ? '\
									<div class="pslp-fm-cc-infos pslp-fm-cc-infos-auto">\
										'+productCredit.deadline+' meses\
									</div>' : '')+'\
							<div class="pslp-fm-second-highlight">\
								<label>Crédito de</label>\
								<strong>R$ '+formatCurrency(productCredit.credit)+'</strong>\
							</div>\
							<a href="javascript:;" class="ps-btn ps-btn-primary pslp-fm-btn pslp-form-init" data-gtm-type="click" data-gtm-clicktype="button" data-gtm-name="Solicite uma proposta">Solicite uma proposta</a>\
							'+(months !== null ? '\
								<div class="pslp-fm-cc-infos">\
									Prazo de '+productCredit.deadline+' meses\
								</div>': '')+'\
						'+(months == null ? '\
						</div>\
						<div class="pslp-fm-main-pj" style="display: none;">\
							<div class="pslp-fm-main-highlight">\
								<label>Valor da parcela</label>\
								<strong>R$ '+formatCurrency(productCredit.feePJ)+'</strong>\
							</div>\
							'+(months == null ? '\
									<div class="pslp-fm-cc-infos pslp-fm-cc-infos-auto">\
										'+productCredit.deadline+' meses\
									</div>' : '')+'\
							<div class="pslp-fm-second-highlight">\
								<label>Crédito de</label>\
								<strong>R$ '+formatCurrency(productCredit.credit)+'</strong>\
							</div>\
							<a href="javascript:;" class="ps-btn ps-btn-primary pslp-fm-btn pslp-form-init" data-gtm-type="click" data-gtm-clicktype="button" data-gtm-name="Solicite uma proposta">Solicite uma proposta</a>\
							'+(months !== null ? '\
								<div class="pslp-fm-cc-infos">\
									Prazo de '+productCredit.deadline+' meses\
								</div>': '')+'\
						</div>' : '')+'\
					</div>\
				</div>';

			feeList_ctt += '\
				<div>\
					<div class="pslp-fm-consorcio-cota">\
						'+(months == null ? '\
						<div class="pslp-fm-main-pf">' : '')+'\
							<div class="pslp-fm-main-highlight">\
								<label>Crédito de</label>\
								<strong>R$ '+formatCurrency(productFee.credit)+'</strong>\
							</div>\
							'+(months == null ? '\
								<div class="pslp-fm-cc-infos pslp-fm-cc-infos-auto">\
									'+productFee.deadline+' meses\
								</div>' : '')+'\
							<div class="pslp-fm-second-highlight">\
								<label>Parcela de</label>\
								<strong>R$ '+formatCurrency(productFee.fee)+'</strong>\
							</div>\
							<a href="javascript:;" class="ps-btn ps-btn-primary pslp-fm-btn pslp-form-init" data-gtm-type="click" data-gtm-clicktype="button" data-gtm-name="Solicite uma proposta">Solicite uma proposta</a>\
							'+(months !== null ? '\
								<div class="pslp-fm-cc-infos">\
									Prazo de '+productFee.deadline+' meses\
								</div>': '')+'\
						'+(months == null ? '\
						</div>\
						<div class="pslp-fm-main-pj" style="display: none;">\
							<div class="pslp-fm-main-highlight">\
								<label>Crédito de</label>\
								<strong>R$ '+formatCurrency(productFee.credit)+'</strong>\
							</div>\
							'+(months == null ? '\
								<div class="pslp-fm-cc-infos pslp-fm-cc-infos-auto">\
									'+productFee.deadline+' meses\
								</div>' : '')+'\
							<div class="pslp-fm-second-highlight">\
								<label>Parcela de</label>\
								<strong>R$ '+formatCurrency(productFee.feePJ)+'</strong>\
							</div>\
							<a href="javascript:;" class="ps-btn ps-btn-primary pslp-fm-btn pslp-form-init" data-gtm-type="click" data-gtm-clicktype="button" data-gtm-name="Solicite uma proposta">Solicite uma proposta</a>\
							'+(months !== null ? '\
								<div class="pslp-fm-cc-infos">\
									Prazo de '+productFee.deadline+' meses\
								</div>': '')+'\
						</div>' : '')+'\
					</div>\
				</div>';
		}

		selectCredit.html(selectCredit_ctt);
		selectFee.html(selectFee_ctt);
		creditList.html(creditList_ctt);
		feeList.html(feeList_ctt);

		$(".consorcio-credito-change, .consorcio-parcela-change").off("change.pslp_consorcio_change").on("change.pslp_consorcio_change",function() {
			var t = $(this),
				idx = $("option",t).index($("option:selected",t)),
				destination = t.is(".consorcio-credito-change") ? ".pslp-fm-consorcio-credito-cotas" : ".pslp-fm-consorcio-parcela-cotas";

			$(destination).slick("slickGoTo",idx);
		});

		$(".pslp-switcher").off("click.pslp_consorcio_click").on("click.pslp_consorcio_click",function() {
			var ctt = $(this).parents(".ps-tab-content"),
				selectOptions = $(".pslp-fm-consorcio-select option",ctt),
				pf = $(".pslp-fm-main-pf",ctt),
				pj = $(".pslp-fm-main-pj",ctt),
				mode = $(this).data("switch");

			selectOptions.each(function() {
				var current = $(this).text(),
					next = $(this).data("switchText");

				$(this).text(next).data("switchText",current);
			});

			ctt.find(".pslp-switcher").removeClass("ps-btn-primary");
			$(".pslp-switcher-"+mode,ctt).addClass("ps-btn-primary");

			if(mode == "pf") {
				pj.hide();
				pf.fadeIn("fast");
				$("#ps-frm-pessoa-pf1").trigger("click");
			} else {
				pf.hide();
				pj.fadeIn("fast");
				$("#ps-frm-pessoa-pj1").trigger("click");
			}

			// if($(this).is(".pslp-switcher-credito"))
			// 	$(".pslp-switcher-parcela-"+mode).trigger("click");
			// else
			// 	$(".pslp-switcher-credito-"+mode).trigger("click");
		});

		var slickParams = {
				slidesToScroll: 1,
				dots: true,
				centerMode: true,
				arrows: true,
				initialSlide: 0,
				mobileFirst: true,
				infinite: true,
				responsive: [
					{
						breakpoint: 767,
						settings: {
							centerPadding: '100px'
						}
					}
				]
			},
			sliderCredito = ".pslp-fm-consorcio-credito-cotas",
			sliderParcela = ".pslp-fm-consorcio-parcela-cotas";

		psLP.SlickSlides(sliderCredito,slickParams);
		psLP.SlickSlides(sliderParcela,slickParams);

		$(sliderCredito).on("afterChange",function(slide,idx) {
			$(".consorcio-credito-change").find("option:eq("+idx.currentSlide+")").prop("selected",true);
		});

		$(sliderParcela).on("afterChange",function(slide,idx) {
			$(".consorcio-parcela-change").find("option:eq("+idx.currentSlide+")").prop("selected",true);
		});


		psLP.FormInitBinding(".consorcio-simulator");
	},
};

var psLib = {
	IsMobile: false,
	IsTablet: false,
	IsTabletPortrait: false,
	IsDesktop: false,
	IsHD: false,
	isOldIE: false,
	DetectMobile: function(userAgent) {
		var mobile = {};

		// valores do http://detectmobilebrowsers.com/
		mobile.detectMobileBrowsers = {
			fullPattern: /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i,
			shortPattern: /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
		};

		return mobile.detectMobileBrowsers.fullPattern.test(userAgent) ||
			mobile.detectMobileBrowsers.shortPattern.test(userAgent.substr(0,4));
	},
	DetectTablet: function(userAgent) {
		var tablets = {};

		// valores do http://detectmobilebrowsers.com/
		tablets.detectMobileBrowsers = {
			tabletPattern: /android|ipad|playbook|silk/i
		};

		return tablets.detectMobileBrowsers.tabletPattern.test(userAgent);
	},
	SetScreen: function() {
		var w = $(window).innerWidth();

		if(w > 990) psLib.IsDesktop = true;
		if(w > 1206) psLib.IsHD = true;

		if(psLib.DetectMobile(navigator.userAgent))
			psLib.IsMobile = true;

		if(psLib.DetectTablet(navigator.userAgent)) {
			psLib.IsTablet = true;

			var orientation = window.matchMedia("(orientation: portrait)").matches;

			if(orientation)
				psLib.IsTabletPortrait = true;
			else
				psLib.IsTabletPortrait = false;

			window.addEventListener("orientationchange", function() {
				if(screen.orientation.angle == 0)
					psLib.IsTabletPortrait = true;
				else
					psLib.IsTabletPortrait = false;
			});
		}

		if(navigator.appVersion.indexOf("MSIE 8") > -1) {
			psLib.IsMobile = false;
			psLib.IsTablet = false;
			psLib.IsDesktop = true;
			psLib.IsOldIE = true;
			psLib.IsHD = false;
			$("html").addClass("lt-ie9");
		}
	},
	ScrollTo: function(pos,interval,callback) {
		if(typeof interval == "undefined") interval = 200;

		if($(".ps-site-top-mob-fixed").length > 0)
			pos = pos - $(".ps-site-top-mob-fixed").outerHeight();
		else if($(".ps-site-top-fixed").length > 0)
			pos = pos - $(".ps-site-top-fixed").outerHeight();

		$("html,body").animate({
			scrollTop: pos
		},interval,callback);
	},
	Modal: function(ctn) {
		$(".ps-open-modal",ctn).off("click.pslib").on("click.pslib",function(e) {
			e.preventDefault();

			var t = $(this),
			d = typeof t.data("modal") != "undefined" ? t.data("modal") : false,
			backdrop = typeof t.data("modalbackdropstatic") != "undefined" ? t.data("modalbackdropstatic") : false;
			keyboard = typeof t.data("modalkeyboarddisable") != "undefined" ?  t.data("modalkeyboarddisable") : true,
			onShow = typeof t.data("modalonshow") != "undefined" ? t.data("modalonshow") : false,
			onHide = typeof t.data("modalonhide") != "undefined" ? t.data("modalonhide") : false;

			if(d != false) {
				if(!psLib.IsMobile) {
					if($(".ps-modal-container",d).length > 0) {
						var obj = t[0].getBoundingClientRect(),
							mWidthClass = $(".ps-modal-container",d).attr("class"),
							ctt = "";

						mWidthClass = $.trim(mWidthClass.replace("ps-modal-container",""));
						
						t.addClass("ps-button-transition-modal");
						ctt = '<div class="ps-transition-modal '+mWidthClass+'" style="width:'+obj.width+'px;height:'+obj.height+'px;left:'+obj.left+'px;top:'+obj.top+'px;"></div>';
						t.after(ctt);

						window.setTimeout(function() {
							mHeight = $(d).find(".ps-modal-container").outerHeight(),
							t.addClass("ps-button-transition-modal");
							
							$(".ps-transition-modal").addClass("ps-transition-modal-open").css("cssText","height: "+mHeight+"px !important");

							//if(mHeight > window.innerHeight)
							//	$(".ps-modal-container",d).addClass("ps-modal-equalize-top");
						},50);

						window.setTimeout(function() {
							$(".ps-transition-modal").remove();
						},1000);
					}
				}

				psLib.ModalShowHide(d,backdrop,keyboard,onShow,onHide);
			}
		});

		$(".ps-modal-close",ctn).off("click.pslib").on("click.pslib",function(e) {
			e.preventDefault();

			var t = $(this),
				modalClose = t.parent().hasClass("ps-modal-container") ? t.parent().parent() : t.parent(),
				modal = typeof t.data("modal") != "undefined" ? t.data("modal") : modalClose;
			psLib.ModalShowHide(modal);
		});
	},
	ModalShowHide: function(dest,backdrop,keyboard,onShow,onHide) {
		if(typeof dest == "undefined" || !$(dest).length) {
			console.warn("Modal inexistente");
			return false;
		}
		var modal = $(dest),
		mode = modal.is(":visible") ? "hide" : "show";

		if(typeof keyboard == "undefined") keyboard = true;
		if(typeof backdrop == "undefined") backdrop = false;
		if(typeof onShow == "undefined") onShow = false;
		if(typeof onHide == "undefined") onHide = false;

		if(mode == "show") {
			modal.show();
			setTimeout(function() {
				if(backdrop) modal.addClass("ps-modal-backdrop-static");
				else modal.removeClass("ps-modal-backdrop-static");

				modal.addClass("ps-modal-visible").data({
					"modalonhide": onHide
				});
			},100);

			var title = modal.find(".ps-modal-title").length ? modal.find(".ps-modal-title").outerHeight() : 0,
			foot = modal.find(".ps-modal-foot").length ? modal.find(".ps-modal-foot").outerHeight() : 0,
			windowH = $(window).height();
			
			if(psLib.IsMobile)
				modal.find(".ps-modal-content").css("max-height",(windowH-title-foot)+"px");
			else 
				modal.find(".ps-modal-content").css("padding-bottom",(foot+14)+"px");

			if(keyboard) {
				$(window).off("keyup.modal_pslib").off("keyup.modal_pslib").on("keyup.modal_pslib",function(e) {
					if(e.keyCode == 27) {
						psLib.ModalShowHide(dest);
						$(this).off("keyup.modal_pslib");
					}
				});
			}

			$("body").css("overflow","hidden");

			if(onShow) eval(onShow);
		} else {
			var callback = typeof modal.data("modalonhide") != "undefined" ? modal.data("modalonhide") : false;

			modal.removeClass("ps-modal-visible").removeData("modalonhide");
			$(window).off("keyup.modal_pslib");

			setTimeout(function() {
				modal.hide();
				if(callback) eval(callback);
			},200);

			$("body").css("overflow","auto");
		}
	},
	Tabs: function(ctn) {
		$(".ps-tab",ctn).off("click.pslib").on("click.pslib",function(e) {
			e.preventDefault();

			var t = $(this),
			d = t.attr("href"),
			p = t.parent().parent(),
			ctt = p.next(".ps-tab-content").find(".ps-tab-content-item"),
			tabSelectedClass = "ps-tab-selected";

			p.find(".ps-tab").removeClass(tabSelectedClass);
			t.addClass(tabSelectedClass);

			ctt.each(function() {
				if($(this).attr("id") != d.replace("#","")) {
					$(this).hide();

					var callback = typeof $(this).data("ontabhide") != "undefined" ? $(this).data("ontabhide") : false;
					if(callback) eval(callback);
				}
			});

			$(d).fadeIn(200,function() {
				var callback = typeof $(this).data("ontabshow") != "undefined" ? $(this).data("ontabshow") : false;
				if(callback) eval(callback);
			});
		});

		if(location.hash != "")
			$("a.ps-tab[href='"+location.hash+"']",ctn).trigger("click.pslib");
	},
	Loading: function(ctn) {
		$(".ps-ico-loading-bar",ctn).each(function() {
			var t = $(this),
			ctt = '<div class="ps-ico-bar-container"><div class="ps-ico-bar-spinner"></div></div>';

			t.html(ctt);
			var spin = t.find(".ps-ico-bar-spinner");
			psLib.LoadingBarRotate(spin);
		});
	},
	LoadingBarRotate: function(obj) {
		var ctt = obj.parent();

		$(obj).animate({
			left: ctt.width()
		}, 1500, function() {
			obj.css("left", -(obj.width()) + "px");
			psLib.LoadingBarRotate(obj);
		});
	},
	Init: function(container) {
		psLib.SetScreen();
		psLib.Modal(container);
		psLib.Tabs(container);
	}
};

!function(root, factory) {
    "function" == typeof define && define.amd ? // AMD. Register as an anonymous module unless amdModuleId is set
    define([], function() {
        return root.svg4everybody = factory();
    }) : "object" == typeof module && module.exports ? // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory() : root.svg4everybody = factory();
}(this, function() {
    /*! svg4everybody v2.1.9 | github.com/jonathantneal/svg4everybody */
    function embed(parent, svg, target) {
        // if the target exists
        if (target) {
            // create a document fragment to hold the contents of the target
            var fragment = document.createDocumentFragment(), viewBox = !svg.hasAttribute("viewBox") && target.getAttribute("viewBox");
            // conditionally set the viewBox on the svg
            viewBox && svg.setAttribute("viewBox", viewBox);
            // copy the contents of the clone into the fragment
            for (// clone the target
            var clone = target.cloneNode(!0); clone.childNodes.length; ) {
                fragment.appendChild(clone.firstChild);
            }
            // append the fragment into the svg
            parent.appendChild(fragment);
        }
    }
    function loadreadystatechange(xhr) {
        // listen to changes in the request
        xhr.onreadystatechange = function() {
            // if the request is ready
            if (4 === xhr.readyState) {
                // get the cached html document
                var cachedDocument = xhr._cachedDocument;
                // ensure the cached html document based on the xhr response
                cachedDocument || (cachedDocument = xhr._cachedDocument = document.implementation.createHTMLDocument(""), 
                cachedDocument.body.innerHTML = xhr.responseText, xhr._cachedTarget = {}), // clear the xhr embeds list and embed each item
                xhr._embeds.splice(0).map(function(item) {
                    // get the cached target
                    var target = xhr._cachedTarget[item.id];
                    // ensure the cached target
                    target || (target = xhr._cachedTarget[item.id] = cachedDocument.getElementById(item.id)), 
                    // embed the target into the svg
                    embed(item.parent, item.svg, target);
                });
            }
        }, // test the ready state change immediately
        xhr.onreadystatechange();
    }
    function svg4everybody(rawopts) {
        function oninterval() {
            // while the index exists in the live <use> collection
            for (// get the cached <use> index
            var index = 0; index < uses.length; ) {
                // get the current <use>
                var use = uses[index], parent = use.parentNode, svg = getSVGAncestor(parent), src = use.getAttribute("xlink:href") || use.getAttribute("href");
                if (!src && opts.attributeName && (src = use.getAttribute(opts.attributeName)), 
                svg && src) {
                    if (polyfill) {
                        if (!opts.validate || opts.validate(src, svg, use)) {
                            // remove the <use> element
                            parent.removeChild(use);
                            // parse the src and get the url and id
                            var srcSplit = src.split("#"), url = srcSplit.shift(), id = srcSplit.join("#");
                            // if the link is external
                            if (url.length) {
                                // get the cached xhr request
                                var xhr = requests[url];
                                // ensure the xhr request exists
                                xhr || (xhr = requests[url] = new XMLHttpRequest(), xhr.open("GET", url), xhr.send(), 
                                xhr._embeds = []), // add the svg and id as an item to the xhr embeds list
                                xhr._embeds.push({
                                    parent: parent,
                                    svg: svg,
                                    id: id
                                }), // prepare the xhr ready state change event
                                loadreadystatechange(xhr);
                            } else {
                                // embed the local id into the svg
                                embed(parent, svg, document.getElementById(id));
                            }
                        } else {
                            // increase the index when the previous value was not "valid"
                            ++index, ++numberOfSvgUseElementsToBypass;
                        }
                    }
                } else {
                    // increase the index when the previous value was not "valid"
                    ++index;
                }
            }
            // continue the interval
            (!uses.length || uses.length - numberOfSvgUseElementsToBypass > 0) && requestAnimationFrame(oninterval, 67);
        }
        var polyfill, opts = Object(rawopts), newerIEUA = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/, webkitUA = /\bAppleWebKit\/(\d+)\b/, olderEdgeUA = /\bEdge\/12\.(\d+)\b/, edgeUA = /\bEdge\/.(\d+)\b/, inIframe = window.top !== window.self;
        polyfill = "polyfill" in opts ? opts.polyfill : newerIEUA.test(navigator.userAgent) || (navigator.userAgent.match(olderEdgeUA) || [])[1] < 10547 || (navigator.userAgent.match(webkitUA) || [])[1] < 537 || edgeUA.test(navigator.userAgent) && inIframe;
        // create xhr requests object
        var requests = {}, requestAnimationFrame = window.requestAnimationFrame || setTimeout, uses = document.getElementsByTagName("use"), numberOfSvgUseElementsToBypass = 0;
        // conditionally start the interval if the polyfill is active
        polyfill && oninterval();
    }
    function getSVGAncestor(node) {
        for (var svg = node; "svg" !== svg.nodeName.toLowerCase() && (svg = svg.parentNode); ) {}
        return svg;
    }
    return svg4everybody;
});