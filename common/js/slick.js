$(function () {

    var consorcioDeadline = 200,
        consorcioTable = [
            {
                "ref": 200,
                "credit": 55000.00,
                "fee": 349.00
            },
            {
                "ref": 200,
                "credit": 60000.00,
                "fee": 380.73
            },
            {
                "ref": 200,
                "credit": 65000.00,
                "fee": 412.46
            },
            {
                "ref": 200,
                "credit": 70000.00,
                "fee": 444.18
            },
            {
                "ref": 200,
                "credit": 75000.00,
                "fee": 475.91
            },
            {
                "ref": 200,
                "credit": 80000.00,
                "fee": 507.64
            },
            {
                "ref": 200,
                "credit": 85000.00,
                "fee": 539.37
            },
            {
                "ref": 200,
                "credit": 90000.00,
                "fee": 571.09
            },
            {
                "ref": 200,
                "credit": 95000.00,
                "fee": 602.82
            },
            {
                "ref": 200,
                "credit": 100000.00,
                "fee": 634.55
            },
            {
                "ref": 200,
                "credit": 110000.00,
                "fee": 698.00
            },
            {
                "ref": 200,
                "credit": 120000.00,
                "fee": 761.45
            },
            {
                "ref": 200,
                "credit": 130000.00,
                "fee": 824.91
            },
            {
                "ref": 200,
                "credit": 140000.00,
                "fee": 888.36
            },
            {
                "ref": 200,
                "credit": 150000.00,
                "fee": 951.82
            },
            {
                "ref": 200,
                "credit": 160000.00,
                "fee": 1015.27
            },
            {
                "ref": 200,
                "credit": 170000.00,
                "fee": 1078.73
            },
            {
                "ref": 200,
                "credit": 180000.00,
                "fee": 1142.18
            },
            {
                "ref": 200,
                "credit": 190000.00,
                "fee": 1205.64
            },
            {
                "ref": 200,
                "credit": 200000.00,
                "fee": 1269.09
            },
            {
                "ref": 200,
                "credit": 210000.00,
                "fee": 1332.54
            },
            {
                "ref": 200,
                "credit": 220000.00,
                "fee": 1396.00
            },
            {
                "ref": 200,
                "credit": 230000.00,
                "fee": 1459.45
            },
            {
                "ref": 200,
                "credit": 240000.00,
                "fee": 1522.91
            },
            {
                "ref": 200,
                "credit": 250000.00,
                "fee": 1546.54
            },
            {
                "ref": 200,
                "credit": 260000.00,
                "fee": 1608.40
            },
            {
                "ref": 200,
                "credit": 270000.00,
                "fee": 1670.26
            },
            {
                "ref": 200,
                "credit": 280000.00,
                "fee": 1732.12
            },
            {
                "ref": 200,
                "credit": 290000.00,
                "fee": 1793.98
            },
            {
                "ref": 200,
                "credit": 300000.00,
                "fee": 1855.85
            },
            {
                "ref": 200,
                "credit": 310000.00,
                "fee": 1917.71
            },
            {
                "ref": 200,
                "credit": 320000.00,
                "fee": 1979.57
            },
            {
                "ref": 200,
                "credit": 330000.00,
                "fee": 2041.43
            },
            {
                "ref": 200,
                "credit": 340000.00,
                "fee": 2103.29
            },
            {
                "ref": 200,
                "credit": 350000.00,
                "fee": 2165.15
            },
            {
                "ref": 200,
                "credit": 360000.00,
                "fee": 2227.01
            },
            {
                "ref": 200,
                "credit": 370000.00,
                "fee": 2288.88
            },
            {
                "ref": 200,
                "credit": 380000.00,
                "fee": 2350.74
            },
            {
                "ref": 200,
                "credit": 390000.00,
                "fee": 2412.60
            },
            {
                "ref": 200,
                "credit": 400000.00,
                "fee": 2474.46
            },
            {
                "ref": 200,
                "credit": 410000.00,
                "fee": 2536.32
            },
            {
                "ref": 200,
                "credit": 420000.00,
                "fee": 2598.18
            },
            {
                "ref": 200,
                "credit": 430000.00,
                "fee": 2660.04
            },
            {
                "ref": 200,
                "credit": 440000.00,
                "fee": 2721.91
            },
            {
                "ref": 200,
                "credit": 450000.00,
                "fee": 2783.77
            },
            {
                "ref": 200,
                "credit": 460000.00,
                "fee": 2845.63
            },
            {
                "ref": 200,
                "credit": 470000.00,
                "fee": 2907.49
            },
            {
                "ref": 200,
                "credit": 480000.00,
                "fee": 2969.35
            },
            {
                "ref": 200,
                "credit": 490000.00,
                "fee": 3031.21
            },
            {
                "ref": 200,
                "credit": 500000.00,
                "fee": 3093.08
            }
        ];

    var consorcioCredito = $('.consorcio-credito-change'),
        consorcioParcela = $('.consorcio-parcela-change');

    var slick1 = $('.slick-1'),
        slick2 = $('.slick-2');

    var formatter = new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'USD',
    });

    consorcioTable.forEach(function (data, i) {

        // Por crédito
        consorcioCredito.append(' <option value="' + i + '" data-index="' + i + '" class="consorcio-credito-change">' + new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(data.credit) + '</option>');
        var slickhtml = '';
        slickhtml += '<div class="pslp-fm-consorcio-cotas pslp-fm-consorcio-credito-cotas">';
        slickhtml += '<div class="pslp-fm-consorcio-cota">';
        slickhtml += '<div class="pslp-fm-main-highlight"><label>Crédito de</label> <strong>' + new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(data.credit) + '</strong></div>';
        slickhtml += '<div class="pslp-fm-second-highlight"><label>PARCELA DE</label> <strong>' + new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(data.fee) + '</strong></div>';
        slickhtml += '<a href="javascript:;" data-index="' + i + '" class="btn-call-modal ps-btn ps-btn-primary pslp-fm-btn pslp-form-init">Solicite uma proposta</a>';
        slickhtml += '<div class="pslp-fm-cc-infos">Prazo de ' + data.ref + ' meses</div>';
        slickhtml += '</div></div>';
        slick1.append(slickhtml);

        // por parcela

        consorcioParcela.append(' <option value="' + i + '" data-index="' + i + '" class="consorcio-credito-change">' + new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(data.fee) + '</option>');
        slickhtml = '<div class="pslp-fm-consorcio-cotas pslp-fm-consorcio-credito-cotas">';
        slickhtml += '<div class="pslp-fm-consorcio-cota">';
        slickhtml += '<div class="pslp-fm-main-highlight"><label>Valor da parcela</label> <strong>' + new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(data.fee) + '</strong></div>';
        slickhtml += '<div class="pslp-fm-second-highlight"><label>Crédito de</label> <strong>' + new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(data.credit) + '</strong></div>';
        slickhtml += '<a href="javascript:;" data-index="' + i + '" class="btn-call-modal ps-btn ps-btn-primary pslp-fm-btn pslp-form-init">Solicite uma proposta</a>';
        slickhtml += '<div class="pslp-fm-cc-infos">Prazo de ' + data.ref + ' meses</div>';
        slickhtml += '</div></div>';
        slick2.append(slickhtml);

    });

    [slick1, slick2].forEach(function (s) {
        s.slick({
            centerMode: true,
            centerPadding: '5px',
            slidesToShow: 3,
            lazyLoad: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        centerMode: true,
                        slidesToShow: 1,
                        lazyLoad: true,
                        rows: 1,
                        arrows: false
                    }
                }
            ]
        });
    });

    slick1.on("afterChange", function (event, slick, currentSlide) {
        consorcioCredito.val(currentSlide)
    });
    slick2.on("afterChange", function (event, slick, currentSlide) {
        consorcioParcela.val(currentSlide)
    });

    consorcioCredito.on('change', function () {
        slick1.slick('slickGoTo', $(this).val());
    });
    consorcioParcela.on('change', function () {
        slick2.slick('slickGoTo', $(this).val());
    });


    /*
    * Trocar abas aba de crédito e aba de parcela
    * */
    var linkAbaCredito = $("a[href$='aba-credito']"),
        linkAbaParcela = $("a[href$='aba-parcela']");

    linkAbaCredito.on('click', function () {
        linkAbaParcela.removeClass('ps-tab-selected');
        $("#aba-parcela").fadeOut(300, function () {
            slick1.slick('slickGoTo', 0);
            $("#aba-credito").fadeIn(300, function () {
                linkAbaCredito.addClass('ps-tab-selected');
            });
        });
    });

    linkAbaParcela.on('click', function () {
        linkAbaCredito.removeClass('ps-tab-selected');
        $("#aba-credito").fadeOut(300, function () {
            slick2.slick('slickGoTo', 0);
            $("#aba-parcela").fadeIn(300, function () {
                linkAbaParcela.addClass('ps-tab-selected');
            });
        });
    });

    $('.btn-call-modal').on('click', function () {
        var creditVal = new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(consorcioTable[$(this).data('index')].credit);

        console.log(creditVal);

        $('.val-credit').html(creditVal);
        $('.value-credit').val(creditVal);
        $('.modal').fadeIn();
    });

    $('.close-modal').on('click', function () {
        $('.modal').fadeOut(200);
    });

// Mascaras de whatsapp
    $('input[name="whatsapp"], input[name="phone"]').mask('(00) 00000-0000');
    $('input[name="datetime"]').mask('00/00 00h00');

    /**
     * Scroll to specific id page
     **/
    jQuery("[data-scrollto]").click(function () {

        /**
         * Variável que puxa valor dentro do data-scrollto
         */
        var seletor = jQuery(this).attr("data-scrollto");

        /**
         * Animação scroll
         */
        jQuery('html, body').animate(
            {
                scrollTop: jQuery(seletor).offset().top
            }, 300
        );
    });

});


// validando formulário

$(".formSend").on('submit', function () {
    if (
        $(this).find('input[name="whatsapp"], input[name="phone"]').val().length < 14
    ) {
        alert('Número incompleto');
        return false;
    }
});
